# tplove-snippets README

This extension provides Lua snippets for TPLove.

Currently, the following snippets are available:

### Requires
| Trigger  | Content |
| -------: | ------- |
| `TPRequire→`   | requires a module from the TPLibrary `local TPStyle = require("tpcoronalibrary.TPStyle")`|
| `TPLua→`   | requires a module from TPLua `local Table = require("TPLua.Table")` |
| `TPLove→`   | requires a module from TPLove `local Promise = require("tplove.Promise")` |

### Class helpers
| Trigger  | Content |
| -------: | ------- |
| `newClassLib→`   | Creates the base structure for a new class. Best used in an empty file |
| `newMethod→`   | Creates a method inside a class  `function MyClass:save() end` |
| `newPrivateMethod→`   | Creates a private method inside a class  `function MyClass:_save() end` |
| `newStaticMethod→`   | Creates a static method inside a class  `function MyClass.identity() end` |
| `newMixin→`   | Creates the base structure for a new mixin. Best used in an empty file |

### Code Tags
| Trigger  | Content |
| -------: | ------- |
| `debtCodeTag→`   | Adds a technical debt code tag  `[DEBT]Technical debt http://mydebt.link [/DEBT]` |
| `tempCodeTag→`   | Adds a temporary code tag  `[TEMP]Temporary code, remove as soon as possible[/TEMP]` |
| `mockCodeTag→`   | Adds a mock code tag  `[MOCK]Mocking return value to 0[/MOCK]` |
| `tbdCodeTag→`   | Adds a to be defined code tag  `[TBD]Implement Analytics here[/TBD]` |
