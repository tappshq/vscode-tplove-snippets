# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.0] - 2019-01-28
- Added TPLove snippet.

## [1.3.3] - 2018-11-22
- Changed TPRequire to not use configTP.libPath.

## [1.3.2] - 2018-08-31
- Fixes "New Mixin" snippet description.

## [1.3.1] - 2018-08-31
- Typo fix.

## [1.3.0] - 2018-08-31
- Added newMixin.

## [1.2.0] - 2018-02-28
- Added code tag snippets for the following tags:
    - Technical Debt
    - Temporary Code
    - Mock Code
    - TBD (To Be Defined) Code

## [1.1.0] - 2018-02-14
- Added newPrivateMethod.
- Rename `arg` to `params` as default function argument, since `arg` is a standard lua global.
- Add ldoc strings to newMethod and newStaticMethod.

## [1.0.1] - 2018-02-01
- Improved README.

## [1.0.0] - 2018-01-31
- Initial release
